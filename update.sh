#!/bin/bash
set -e

PRIVILEGED_APPS=(
  "com.google.android.gms"
  "com.android.vending"
)

if ! which aapt; then
    echo "No aapt binary found; run \`mm aapt\` first"
    exit 1
fi

# Always execute in the script's directory
pushd "$(dirname "$(realpath "$0")")"

# Clean up all previously generated
rm -rf Android.mk apps.mk bin lib

repo="https://f-droid.org/repo/"
prebuilt_libs=()

contains() {
  local e match="$1"
  shift
  for e; do [[ "$e" == "$match" ]] && return 0; done
  return 1
}

addPrebuiltJniModules() {
    local lib=""
    local app_mod=""
    for lib_mod_name in "${prebuilt_libs[@]}"; do
        lib="$(awk -F'__' '{ print $2 }' <<< "$lib_mod_name")"
        app_mod="$(awk -F'__' '{ print $1 }' <<< "$lib_mod_name")"
        app_dir="app"
        if contains "$app_mod" "${PRIVILEGED_APPS[@]}"; then
            app_dir="priv-app"
        fi
        cat >> Android.mk <<EOF
include \$(CLEAR_VARS)
LOCAL_MODULE := $lib_mod_name
LOCAL_MODULE_STEM := $lib
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES_arm := lib/armeabi-v7a/$lib
LOCAL_SRC_FILES_arm64 := lib/arm64-v8a/$lib
LOCAL_SRC_FILES_x86 := lib/x86/$lib
LOCAL_SRC_FILES_x86_64 := lib/x86_64/$lib
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_CHECK_ELF_FILES := false
LOCAL_PRODUCT_MODULE := true
LOCAL_STRIP_MODULE := false
LOCAL_MODULE_PATH := \$(TARGET_OUT_PRODUCT)/$app_dir/$app_mod/lib/\$(TARGET_ARCH)/
include \$(BUILD_PREBUILT)
EOF
    done
}

addJNI() {
    unzip -o bin/$1 'lib/*' || return 0
    local local_prebuilt_libs=()
    local libname=""
    while IFS= read -r line; do
        libname=$(echo -e "$line" | awk '{print $4}' | sed -E 's@lib/[^/]+/(.*)@\1@')
        [ -z "$libname" ] && continue
        # Prepend the app module name in the JNI libary module name
        # This will be used in addPrebuiltJniModules to install the JNI
        # to the app's private lib directory
        libname="$2__$libname"
        contains "$libname" "${local_prebuilt_libs[@]}" && continue
        local_prebuilt_libs+=("${libname}")
    done <<< "$(unzip -l -qq bin/$1 'lib/*')"
    if [[ ! -z "$local_prebuilt_libs" ]]; then
        addition+=$'\n'
        addition+="LOCAL_REQUIRED_MODULES := ${local_prebuilt_libs[@]}"
        prebuilt_libs+=("${local_prebuilt_libs[@]}")
    fi
}

addCopy() {
    addition=""
    if contains "$2" "${PRIVILEGED_APPS[@]}"; then
        addition="LOCAL_PRIVILEGED_MODULE := true"
    fi

    addJNI $1 $2

cat >> Android.mk <<EOF
include \$(CLEAR_VARS)
LOCAL_MODULE := $2
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := bin/$1
LOCAL_MODULE_CLASS := APPS
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_OVERRIDES_PACKAGES := $3
LOCAL_PRODUCT_MODULE := true
LOCAL_REPLACE_PREBUILT_APK_INSTALLED := \$(LOCAL_PATH)/bin/$1
$addition
$(aapt d badging "bin/$1" |sed -nE "s/uses-library-not-required:'(.*)'/LOCAL_OPTIONAL_USES_LIBRARIES += \1/p")
$(aapt d badging "bin/$1" |sed -nE "s/uses-library:'(.*)'/LOCAL_USES_LIBRARIES += \1/p")
include \$(BUILD_PREBUILT)
EOF
echo -e "\t$2 \\" >> apps.mk
}

addMultiarch() {
    addition=""
    # Use the arm64 apk as an example to generate LOCAL_PREBUILT_JNI_LIBS only
    addJNI $1_$2_arm64.apk $1
    # Extract JNIs for all the other architectures as well
    for apk in bin/$1_$2_*.apk; do
        [[ "$apk" == bin/$1_$2_arm64.apk ]] && continue
        unzip -o $apk 'lib/*' || continue
    done
    cat >> Android.mk <<EOF
include \$(CLEAR_VARS)
LOCAL_MODULE := $1
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := APPS
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_OVERRIDES_PACKAGES := $3
LOCAL_PRODUCT_MODULE := true
LOCAL_SRC_FILES := bin/\$(LOCAL_MODULE)_$2_\$(my_src_arch).apk
LOCAL_REPLACE_PREBUILT_APK_INSTALLED := \$(LOCAL_PATH)/bin/\$(LOCAL_MODULE)_$2_\$(my_src_arch).apk
$addition
$(aapt d badging "bin/$1_$2_arm64.apk" |sed -nE "s/uses-library-not-required:'(.*)'/LOCAL_OPTIONAL_USES_LIBRARIES += \1/p")
$(aapt d badging "bin/$1_$2_arm64.apk" |sed -nE "s/uses-library:'(.*)'/LOCAL_USES_LIBRARIES += \1/p")
include \$(BUILD_PREBUILT)
EOF
echo -e "\t$1 \\" >> apps.mk
}

rm -Rf apps.mk lib
cat > Android.mk <<EOF
LOCAL_PATH := \$(my-dir)
my_archs := arm arm64 x86 x86_64
my_src_arch := \$(call get-prebuilt-src-arch, \$(my_archs))
EOF
echo -e 'PRODUCT_PACKAGES += \\' > apps.mk

mkdir -p bin

#downloadFromFdroid packageName overrides
downloadFromFdroid() {
    mkdir -p tmp
    [ "$oldRepo" != "$repo" ] && rm -f tmp/index.xml
    oldRepo="$repo"
    if [ ! -f tmp/index.xml ];then
        #TODO: Check security keys
        wget --connect-timeout=10 $repo/index.jar -O tmp/index.jar
        unzip -p tmp/index.jar index.xml > tmp/index.xml
    fi
    marketversion="$(xmlstarlet sel -t -m '//application[id="'"$1"'"]' -v ./marketversion tmp/index.xml || true)"
    nativecodes="$(xmlstarlet sel -t -m '//application[id="'"$1"'"]/package[version="'"$marketversion"'"]' -v nativecode -o ' ' tmp/index.xml || true)"

    # If packages have separate nativecodes
    if echo "$nativecodes" |grep -q arm && ! echo "$nativecodes" |grep -q ',' ;then
        for native in $nativecodes;do
            newNative="$(echo $native |sed -e s/arm64-v8a/arm64/g -e s/armeabi-v7a/arm/g)"
            apk="$(xmlstarlet sel -t -m '//application[id="'"$1"'"]/package[version="'"$marketversion"'" and nativecode="'"$native"'"]' -v ./apkname tmp/index.xml)"
            localName="${1}_${marketversion}_${newNative}.apk"
            if [ ! -f bin/$localName ];then
                while ! wget --connect-timeout=10 $repo/$apk -O bin/$localName;do sleep 1;done
            fi
        done
        addMultiarch $1 $marketversion "$2"
    else
        apk="$(xmlstarlet sel -t -m '//application[id="'"$1"'"]/package[version="'"$marketversion"'"]' -v ./apkname tmp/index.xml || xmlstarlet sel -t -m '//application[id="'"$1"'"]/package[1]' -v ./apkname tmp/index.xml)"
        if [ ! -f bin/$apk ];then
            while ! wget --connect-timeout=10 $repo/$apk -O bin/$apk;do sleep 1;done
        fi
        addCopy $apk $1 "$2"
	fi
}

# Web browser
downloadFromFdroid org.mozilla.fennec_fdroid "Browser2 QuickSearchBox"
# Calendar
downloadFromFdroid ws.xsoh.etar Calendar
# Pdf viewer
downloadFromFdroid com.artifex.mupdf.viewer.app
# Play Store download
downloadFromFdroid com.aurora.store
# Mail client
downloadFromFdroid com.fsck.k9 "Email"

# Gallery
downloadFromFdroid org.fossify.gallery "Photos Gallery Gallery2"
# Camera
downloadFromFdroid net.sourceforge.opencamera "Camera Camera2"
# Messages
downloadFromFdroid org.fossify.messages messaging
# Contacts
downloadFromFdroid org.fossify.contacts Contacts
# Clock
downloadFromFdroid com.vicolo.chrono "DeskClock AlarmClock"

downloadFromFdroid com.machiav3lli.fdroid

repo=https://microg.org/fdroid/repo/
downloadFromFdroid com.google.android.gms
downloadFromFdroid com.google.android.gsf
downloadFromFdroid com.android.vending

addPrebuiltJniModules

echo >> apps.mk

rm -Rf tmp
